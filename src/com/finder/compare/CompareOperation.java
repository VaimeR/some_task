package com.finder.compare;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @since 25.02.18
 * @author vaimer
 */
public class CompareOperation
{
    /**
     * Find classPath from tokens by rules
     * @param rules - list of rules to checking tokens
     * @param tokens - map of tokens format<classPath, classNames>
     * @return - classpath conforming to the rules of the token
     */
    public List<String> findClasses(List<String> rules, Map<String, String> tokens)
    {
        return tokens.keySet()
                     .stream()
                     .filter(key -> isAcceptByRules(rules, tokens.get(key)))
                     .sorted()
                     .collect(Collectors.toList());
    }

    /**
     * Check token to accept rules
     * @param rules - List of rules
     * @param token - token to check
     * @return - true if accept by all rules
     */
    private boolean isAcceptByRules(List<String> rules, String token)
    {
        List<String> classNameWords = getClassNameWords(token);

        boolean result = true;

        for(int i = 0; i < rules.size(); i++)
        {
            if(rules.get(i).equals(" ") &&
               classNameWords.size() != i)
            {
                return false;
            }
            else if(isRuleHaveStar(rules.get(i)))
            {
                result &= isWordAcceptByStarRule(rules.get(i), classNameWords.get(i));
            }
            else if(classNameWords.size() - 1 >= i)
            {
                result &= classNameWords.get(i).contains(rules.get(i));
            }
        }

        return result;
    }

    /**
     * This method check the word by rule with star
     * @param rule - rule with star
     * @param word - word to check
     * @return - true or false if is accept by rule
     */
    private boolean isWordAcceptByStarRule(String rule, String word)
    {
        String[] rulePaths = rule.split("\\*");

        // Try to check symbols of word
        String firstPathToCheck = word.substring(0, rulePaths[0].length());
        String secondPathToCheck = word.substring(word.length() - 1 - rulePaths[1].length(),
                                                  word.length());

        return firstPathToCheck.contains(rulePaths[0]) &&
               secondPathToCheck.contains(rulePaths[1]);
    }

    /**
     * Checking star in the rule
     * @param rule - string of rule
     * @return - true/false have or not a star
     */
    private boolean isRuleHaveStar(String rule)
    {
        return rule.contains("*");
    }

    /**
     * This method breaks the token into its constituent words
     * @param token - token to break
     * @return - List of words class name
     */
    private List<String> getClassNameWords(String token)
    {
        char[] charArray = token.toCharArray();

        List<String> words = new ArrayList<>();

        StringBuffer buffer = new StringBuffer();

        int iterator = 0;

        for(char ch : charArray)
        {
            iterator++;

            if(Character.isUpperCase(ch) &&
               buffer.length() > 0)
            {
                words.add(buffer.toString());
                buffer = new StringBuffer();
            }

            buffer.append(ch);

            if(iterator == charArray.length)
            {
                words.add(buffer.toString());
            }
        }

        return words;
    }
}
