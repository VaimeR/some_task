package com.finder;

import com.finder.compare.CompareOperation;
import com.finder.parsers.RulesParseOperation;
import com.finder.parsers.TokenParseOperation;

import static java.lang.System.out;
import static java.nio.file.Files.readAllLines;
import static java.nio.file.Paths.get;

import java.util.List;
import java.util.Map;

/**
 * @since 25.02.18
 * @author vaimer
 */
public class FindClasses
{
    public static void main(String[] args)
    {
        RulesParseOperation rulesParseOperation = new RulesParseOperation();
        TokenParseOperation tokenParseOperation = new TokenParseOperation();
        CompareOperation compareOperation = new CompareOperation();

        try
        {
            List<String> rules = rulesParseOperation.getRules(args[1]);

            Map<String, String> tokens = tokenParseOperation.getTokens(readAllLines(get(args[0])));

            List<String> result = compareOperation.findClasses(rules, tokens);

            out.println(result);
        }
        catch (Exception e)
        {
            out.println(e);
        }
    }
}
