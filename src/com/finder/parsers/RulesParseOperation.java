package com.finder.parsers;

import com.finder.exceptions.ParseRulesException;

import java.util.ArrayList;
import java.util.List;

/**
 * @since 25.02.18
 * @author vaimer
 */
public class RulesParseOperation
{
    /**
     * This method generate list of rules of string rules.
     * String format: FBB, FB*r
     * @param rules - string of potential rules
     * @return - list of pared rules
     * @throws ParseRulesException - throw if rules have bad format
     */
    public List<String> getRules(String rules) throws ParseRulesException
    {
        // Check empty rules string
        if(rules == null || rules.isEmpty())
        {
            throw new ParseRulesException("Pattern is empty!");
        }

        List<String> rulesList = new ArrayList<>();

        char[] charArray = checkRulesToLowerCase(rules).toCharArray();

        StringBuffer buffer = new StringBuffer();
        int iteration = 0;

        for(char ch : charArray)
        {
            iteration++;

            if (ch == '*')
            {
                buffer.append(ch);
            }

            if (ch == ' ')
            {
                // If ' ' is does not last symbol
                if(iteration != charArray.length)
                {
                    throw new ParseRulesException("Invalid rule format!");
                }

                buffer = saveAndEraseBuffer(rulesList, buffer);

                rulesList.add(" ");
            }

            if (Character.isLowerCase(ch))
            {
                buffer.append(ch);
            }

            if (Character.isUpperCase(ch))
            {
                buffer = saveAndEraseBuffer(rulesList, buffer);

                buffer.append(ch);
            }

            // Save last rule
            if(iteration == charArray.length)
            {
                saveAndEraseBuffer(rulesList, buffer);
            }
        }

        return rulesList;
    }

    /**
     * This method save value of string buffer in list, if he does not empty.
     * Erase string buffer
     * @param rulesList - list of parse rules
     * @param buffer - current string buffer
     * @return - clean or current string buffer
     */
    private StringBuffer saveAndEraseBuffer(List<String> rulesList, StringBuffer buffer)
    {
        if(buffer.length() > 0)
        {
            rulesList.add(buffer.toString());

            buffer = new StringBuffer();
        }

        return buffer;
    }

    /**
     * This method checks the string with the rule for all letters in lowercase.
     * @param rules - string of rule
     * @return - return string in UpperCase if all letters in lowercase, or unchanged string
     */
    private String checkRulesToLowerCase(String rules)
    {
        char[] charArray = rules.toCharArray();

        for(char ch : charArray)
        {
            if(Character.isUpperCase(ch))
            {
                return rules;
            }
        }

        return rules.toUpperCase();
    }
}
