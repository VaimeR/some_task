package com.finder.parsers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @since 25.02.18
 * @author vaimer
 */
public class TokenParseOperation
{
    /**
     * This method parse list lines classPath to map of tokens.
     * @param lines - list of parse classpath lines
     * @return - map with format <classPath, ClassName>
     */
    public Map<String, String> getTokens(List<String> lines)
    {
        lines = removeServiceCharactersAndEmptyLines(lines);

        return getClassNames(lines);
    }

    /**
     * This method try to remove empty lines and symbol of ' '
     * @param lines - list of parse classpath lines
     * @return - clean list of parse lines
     */
    private List<String> removeServiceCharactersAndEmptyLines(List<String> lines)
    {
        List<String> clearLines = new ArrayList<>();

        for(String line : lines)
        {
            if(!line.isEmpty())
            {
                String clearLine = line.replace(" ", "");

                clearLines.add(clearLine);
            }
        }

        return clearLines;
    }

    /**
     * This method create a map of class names
     * @param lines - clean list of parse classpath lines
     * @return - map with format <classPath, ClassName>
     */
    private Map<String, String> getClassNames(List<String> lines)
    {
        Map<String, String> tokens = new HashMap<>();

        for(String line : lines)
        {
            String[] classPath = line.split("\\.");

            // Put classPath and last result od split - It is className
            tokens.put(line, classPath[classPath.length - 1]);
        }

        return tokens;
    }
}
