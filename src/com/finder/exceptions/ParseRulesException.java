package com.finder.exceptions;

/**
 * @since 25.02.18
 * @author vaimer
 */
public class ParseRulesException extends Exception
{
    public ParseRulesException(String message)
    {
        super(message);
    }
}
