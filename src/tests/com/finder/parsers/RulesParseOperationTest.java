package finder.parsers;

import com.finder.exceptions.ParseRulesException;
import com.finder.parsers.RulesParseOperation;
import com.sun.deploy.security.ruleset.RuleParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

/**
 * @since 25.02.18
 * @author vaimer
 */
public class RulesParseOperationTest
{
    private RulesParseOperation rulesParseOperation;

    @Before
    public void initTest()
    {
        rulesParseOperation = new RulesParseOperation();
    }

    @Test(expected = RuleParseException.class)
    public void getRulesWithNullRulesString() throws ParseRulesException
    {
        rulesParseOperation.getRules(null);
    }

    @Test(expected = RuleParseException.class)
    public void getRulesWithEmptyRulesString() throws ParseRulesException
    {
        rulesParseOperation.getRules("");
    }

    @Test(expected = RuleParseException.class)
    public void getRulesWithBadFormatRulesString() throws ParseRulesException
    {
        rulesParseOperation.getRules("FB F");
    }

    @Test
    public void getRulesWithLowerCaseRule() throws ParseRulesException
    {
        List<String> rule = rulesParseOperation.getRules("fb");

        List<String> testList = Arrays.asList("F", "B");

        Assert.assertThat(testList, is(rule));
    }

    @Test
    public void getRulesWithStar() throws ParseRulesException
    {
        List<String> rule = rulesParseOperation.getRules("FB*r");

        List<String> testList = Arrays.asList("F", "B*r");

        Assert.assertThat(testList, is(rule));
    }

    @Test
    public void getRulesWithSpace() throws ParseRulesException
    {
        List<String> rule = rulesParseOperation.getRules("FB ");

        List<String> testList = Arrays.asList("F", "B", " ");

        Assert.assertThat(testList, is(rule));
    }
}
