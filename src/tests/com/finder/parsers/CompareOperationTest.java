package finder.parsers;

import com.finder.FindClasses;
import com.finder.compare.CompareOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.Is.is;

public class CompareOperationTest
{
    private CompareOperation compareOperation;

    private Map<String, String> testTokens;
    private List<String> resultFinder = Arrays.asList("c.d.FooBar", "a.b.FooBarBaz");
    List<String> testRules = Arrays.asList("F", "B");

    @Before
    public void initTest()
    {
        compareOperation = new CompareOperation();

        testTokens = new HashMap<>();
        testTokens.put("a.b.FooBarBaz", "FooBarBaz");
        testTokens.put("c.d.FooBar", "FooBar");
    }

    @Test
    public void findClasses()
    {
        Assert.assertThat(resultFinder, is(compareOperation.findClasses(testRules, testTokens)));
    }

    @Test
    public void findClassesWithSpace()
    {
        testRules = Arrays.asList("F", "B", " ");

        testTokens.remove("a.b.FooBarBaz");

        Assert.assertThat(resultFinder, is(compareOperation.findClasses(testRules, testTokens)));
    }

    @Test
    public void findClassesWithStar()
    {
        testRules = Arrays.asList("F", "B*r");

        testTokens.remove("a.b.FooBarBaz");

        Assert.assertThat(resultFinder, is(compareOperation.findClasses(testRules, testTokens)));
    }
}
