package finder.parsers;

import com.finder.parsers.TokenParseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.core.Is.is;

/**
 * @since 25.02.18
 * @author vaimer
 */
public class TokenParseOperationTest
{
    private TokenParseOperation tokenParseOperation;

    private List<String> lines = Arrays.asList("a.b.FooBarBaz", "   c.d.FooBar", "");

    private Map<String, String> testTokens;

    @Before
    public void initTest()
    {
        tokenParseOperation = new TokenParseOperation();

        testTokens = new HashMap<>();
        testTokens.put("a.b.FooBarBaz", "FooBarBaz");
        testTokens.put("c.d.FooBar", "FooBar");
    }

    @Test
    public void getTokensWithEmptyLine()
    {
        Map<String, String> maps= tokenParseOperation.getTokens(Collections.emptyList());

        Assert.assertTrue(maps.isEmpty());
    }

    @Test
    public void getTokens()
    {
        Map<String, String> maps= tokenParseOperation.getTokens(lines);

        Assert.assertThat(testTokens, is(maps));
    }
}
